# CryptoBoard

The simple python CLI tool for one currency to show the current crypto balance yours purchases. That's all.
Data source is from [https://www.cryptocompare.com/](https://www.cryptocompare.com/). Token surprisingly not needed.

# Example

![table](table.png)

# Dependencies

- python
- poetry / requirements
- docker (optional, only for study purpose)

# Run

## create a file of your purchases

```sh
$ touch data.json
```

- Fill the file by `data_example.json` [name of crypto, purchased value, value at main currency at time of purchase]
- Select only one currency. If the purchase was made in different currencies unite them into one currency.

## create env file

```sh
$ cp .env_example .env
```

- Choose the main currency for example `USD`, `EUR`, `CZK`...
- Type right path to your data file.

## Install dependencies (optional)

```sh
$ poetry install
```

## Run

```sh
$ python app/main.py
$ docker-compose up # optional
```

tip:

- You can set alias for running this script from everywhere.
  - set absolute path to the data file at the `.env`

```sh
alias crypto='/path/to/python /path/to/CryptoBoard/app/main.py'
```

# Tests

```sh
$ python -m pytest
```

Pytest is not in the requirements, because it causes an error while installing the docker image.

# License

See [LICENSE](LICENSE)
