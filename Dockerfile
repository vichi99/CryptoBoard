FROM python:3.9-slim as cryptoboard

ARG REQUIREMENTS_FILE=requirements

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install pytest && \
    pip install --upgrade -r ${REQUIREMENTS_FILE}

CMD ["./startup.sh"]
