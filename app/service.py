from dataclasses import dataclass


def calculate_balance_percent(current_value: float, purchase_value: float) -> float:
    """
    :return: Return rounded percent.
    :rtype: float
    """
    return round((100 * current_value) / purchase_value - 100, 2)


@dataclass
class Purchase:
    name: str
    crypto_value: float
    purchase_value: int
    current_value: int

    @property
    def main_balance(self) -> int:
        return round(self.current_value - self.purchase_value, 2)

    @property
    def balance_percent(self) -> float:
        return calculate_balance_percent(self.current_value, self.purchase_value)
