import json
from numbers import Number
from random import randint

import cryptocompare
from service import calculate_balance_percent


def test_balance_percent():
    values = [(randint(100, 2000), randint(100, 2000)) for _ in range(20, 50)]
    for i in values:
        res_1 = round((100 * i[0]) / i[1] - 100, 2)
        res_2 = calculate_balance_percent(i[0], i[1])
        print(res_1)
        assert res_1 == res_2


def test_cryptocompare_api():
    currency_list = ["BTC", "ETH", "SOL", "ADA", "XRP"]
    for i in currency_list:
        cryptocompare.get_price(i, currency="CZK")


def test_data():
    f = open("data/data_example.json", "r")
    data = json.load(f)
    f.close()

    for i in data:
        assert type(i[0]) == str
        assert type(i[1]) == int or float
        assert type(i[2]) == int or float
