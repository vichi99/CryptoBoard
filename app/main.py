import json
import os
from typing import List

import cryptocompare
from dotenv import load_dotenv
from rich.console import Console
from rich.table import Table
from service import Purchase, calculate_balance_percent

load_dotenv(override=True)

# Load data
f = open(os.getenv("DATA_DIR"), "r")
data = json.load(f)
f.close()


class CryptoBoard:
    def __init__(self) -> None:

        # selecting main currency
        self.main_currency = os.getenv("MAIN_CURRENCY")

        # loading currency values from cryptocompare api by input data
        self.currency_values = self.get_currency_values()

        # filling data
        self.purchase_data = self.get_purchase_data()

        self.show_result()

        self.show_sum_currencies()

    def get_currency_values(self):
        """
        The first is making currency list from input data.
        The second is making output dictionary with current
        value for one unitfrom cryptocompare api.

        Output: {'BTC': 1429234, 'ETH': 103848.8,...}

        :return: Return dictionary with crypto name keys. At the values are current value for one unit in the main currency.
        :rtype: dict
        """

        # making currency list from input data
        currency_list = []
        for i in data:
            if i[0] not in currency_list:
                currency_list.append(i[0])

        # by currency list will read current value by the main currency
        ret = dict()
        for i in currency_list:
            r = cryptocompare.get_price(i, currency=self.main_currency)
            ret[i] = round(r[i][self.main_currency], 2)
        return ret

    def _get_current_value(self, crypto_name: str, crypto_value: float) -> float:
        """
        :param crypto_name: Currency name.
        :type crypto_name: str
        :param crypto_value: Crypto value.
        :type crypto_value: float
        :return: Return current value at main currency.
        :rtype: float
        """
        return round(crypto_value * self.currency_values[crypto_name], 2)

    def get_purchase_data(self) -> List[Purchase]:
        """
        From global variable data will make list of purchases.
        Default data are the list of purchases.
        Default purchases:[name of crypto, purchased value, value at main currency]

        :return: Purchases.
        :rtype: List[Purchase]
        """
        purchases = []
        for i in data:
            purchases.append(
                Purchase(
                    name=i[0],
                    crypto_value=i[1],
                    purchase_value=i[2],
                    current_value=self._get_current_value(i[0], i[1]),
                )
            )
        purchases = sorted(purchases, key=lambda k: k.purchase_value, reverse=True)
        return purchases

    def show_result(self):
        """
        Showing result table.
        """

        # making table
        console = Console()
        table = Table(
            title="Purchases", show_header=True, header_style="bold bright_blue"
        )
        table.add_column("Name", style="dim", width=12)
        table.add_column("Amount")
        table.add_column(f"Purchased for {self.main_currency}", justify="right")
        table.add_column(f"Current Value {self.main_currency}", justify="right")
        table.add_column(f"balance {self.main_currency}", justify="right")
        table.add_column("balance %", justify="right")

        # filling table by purchase_data
        purchased_all = 0
        current_all = 0
        for i in self.purchase_data:
            color_balance = "green" if i.main_balance >= 0 else "red"
            table.add_row(
                i.name,
                str(i.crypto_value),
                str(i.purchase_value),
                str(i.current_value),
                f"[{color_balance}]{i.main_balance}[/{color_balance}]",
                f"[{color_balance}]{i.balance_percent}[/{color_balance}]",
            )
            purchased_all += i.purchase_value
            current_all += i.current_value

        # making summarize row
        table.add_row("")

        color_res = "green" if current_all >= purchased_all else "red"

        balance = round(current_all - purchased_all, 2)
        balance_percent = calculate_balance_percent(current_all, purchased_all)

        table.add_row(
            "Summarize",
            "",
            f"[bold]{round(purchased_all,2)}[/bold]",
            f"[{color_res}][bold]{round(current_all,2)}[/bold][/{color_res}]",
            f"[{color_res}][bold]{balance}[/bold][/{color_res}]",
            f"[{color_res}][bold]{balance_percent}[/bold][/{color_res}]",
        )

        console.print(table)

    def show_sum_currencies(self):
        """
        Showing sum of currencies table.
        """
        # making table
        console = Console()
        table = Table(
            title="Sum of currencies", show_header=True, header_style="bold bright_blue"
        )
        table.add_column("Name", style="dim", width=12)
        table.add_column("Amount")
        table.add_column(f"Current Value {self.main_currency}", justify="right")

        summary: dict = {}
        for i in self.purchase_data:
            if i.name not in summary:
                summary[i.name] = {
                    "crypto_value": i.crypto_value,
                    "current_value": i.current_value,
                }
            else:
                summary[i.name]["crypto_value"] = (
                    summary[i.name]["crypto_value"] + i.crypto_value
                )
                summary[i.name]["current_value"] = (
                    summary[i.name]["current_value"] + i.current_value
                )

        for i in sorted(summary):
            table.add_row(
                i,
                str(summary[i]["crypto_value"]),
                str(round(summary[i]["current_value"], 2)),
            )

        console.print(table)


if __name__ == "__main__":
    CryptoBoard()
